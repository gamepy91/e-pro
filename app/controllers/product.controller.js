const Product = require("../models/product.model");

// Retrieve all Product from the database.
exports.findAll = (req, res) => {
  Product.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving product."
      });
    else res.send(data);
  });
};

exports.getCustom = (req, res) => {
  Product.getCustomRuleProductList(req.body.custom_user_ID, req.body.what_component, req.body.component_ID, req.body.extra_query_rule, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving product."
      });
    else res.send(data);
  });
};

exports.upsertCustom = (req, res) => {
  Product.upsertCustomRuleProductList(req.body.custom_user_ID, req.body.what_component, req.body.component_ID, req.body.product_array, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          { ok: "n", error: err.message }
      });
    else res.send(data);
  });
};

exports.deleteCustom = (req, res) => {
  Product.deleteCustomRuleProductList(req.body.custom_user_ID, req.body.what_component, req.body.component_ID, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          { ok: "n", error: err.message }
      });
    else res.send(data);
  });
};