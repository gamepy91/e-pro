const sql = require("./db.js");

// constructor
const Product = function (product) {
    this.id = product.id;
    this.composite_index = product.composite_index;
    this.custom_user_ID = product.custom_user_ID;
    this.what_component = product.what_component;
    this.component_ID = product.component_ID;
    this.product_array = product.product_array;
    this.custom_field = product.active;
};

Product.getAll = result => {
    sql.query("SELECT * FROM tb_custom_rule_product_sorted_list", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("product: ", res);
        result(null, res);
    });
};

Product.getCustomRuleProductList = (custom_user_ID, what_component, component_ID, extra_query_rule, result) => {
    let composite_index = custom_user_ID + what_component + component_ID
    if (typeof ((custom_user_ID) == "number" || typeof (custom_user_ID) == "int") && (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product" || what_component == "promotion")) {
        if (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "promotion") {
            if (component_ID && parseInt(component_ID)) {
                sql.query("SELECT * FROM tb_custom_rule_product_sorted_list", (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    console.log("product: ", res);
                    result(null, res);
                });
            }
            else {
                result({ message: "missing/invalid component_ID" }, null);
            }
        }
        else if (what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product") {
            if (component_ID == "") {
                sql.query("SELECT * FROM tb_custom_rule_product_sorted_list", (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    console.log("product: ", res);
                    result(null, res);
                });
            }
            else {
                result({ message: "component_ID not required" }, null);
            }
        }
    }
    else {
        result({ message: "missing/invalid custom_user_ID or what_component" })
    }
};

Product.upsertCustomRuleProductList = (custom_user_ID, what_component, component_ID, product_array, result) => {
    let composite_index = custom_user_ID + what_component + component_ID
    if (typeof ((custom_user_ID) == "number" || typeof (custom_user_ID) == "int") && (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product" || what_component == "promotion")) {
        if (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "promotion") {
            if (component_ID && parseInt(component_ID)) {
                sql.query("SELECT * FROM tb_custom_rule_product_sorted_list WHERE composite_index = ?", composite_index, (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    if (res.length == 0) {
                        // not found product with the id
                        sql.query("INSERT INTO tb_custom_rule_product_sorted_list(composite_index,custom_user_ID, what_component, component_ID, product_array) VALUES (?, ?, ?, ?,?)", [composite_index, custom_user_ID, what_component, component_ID, product_array], (err, res) => {
                            if (err) {
                                console.log("error: ", err);
                                result(null, err);
                                return;
                            }
                            result(null, { ok: "y ", result: res });
                            return;
                        });
                    }
                    else {
                        sql.query("UPDATE tb_custom_rule_product_sorted_list SET composite_index = ?, custom_user_ID = ?, what_component = ?, component_ID = ?, product_array = ? WHERE composite_index = ?",
                            [composite_index, custom_user_ID, what_component, component_ID, product_array, composite_index], (err, res) => {
                                if (err) {
                                    console.log("error: ", err);
                                    result(null, err);
                                    return;
                                }
                                console.log("update product with id: ", composite_index);
                                result(null, { ok: "y ", result: res });
                                return;
                            });
                    }
                });
            }
            else {
                result({ message: "missing/invalid component_ID" }, null);
            }
        }
        else if (what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product") {
            if (component_ID == "") {
                sql.query("SELECT * FROM tb_custom_rule_product_sorted_list WHERE composite_index = ?", composite_index, (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    if (res.length == 0) {
                        sql.query("INSERT INTO tb_custom_rule_product_sorted_list(composite_index,custom_user_ID, what_component, component_ID, product_array) VALUES (?, ?, ?, ?,?)", [composite_index, custom_user_ID, what_component, component_ID, product_array], (err, res) => {
                            if (err) {
                                console.log("error: ", err);
                                result(null, err);
                                return;
                            }
                            result(null, { ok: "y ", result: res });
                            return;
                        });
                    }
                    else {
                        sql.query("UPDATE tb_custom_rule_product_sorted_list SET composite_index = ?, custom_user_ID = ?, what_component = ?, component_ID = ?, product_array = ? WHERE composite_index = ?",
                            [composite_index, custom_user_ID, what_component, component_ID, product_array, composite_index], (err, res) => {
                                if (err) {
                                    console.log("error: ", err);
                                    result(null, err);
                                    return;
                                }
                                console.log("update product with composite_index: ", composite_index);
                                result(null, { ok: "y ", result: res });
                                return;
                            });
                    }
                });
            }
            else {
                result({ message: "component_ID not required" }, null);
            }
        }
        // result(null, { ok: "y " });
    }
    else {
        result({ message: "missing/invalid custom_user_ID or what_component" })
    }
};

Product.deleteCustomRuleProductList = (custom_user_ID, what_component, component_ID, result) => {
    let composite_index = custom_user_ID + what_component + component_ID
    if (typeof ((custom_user_ID) == "number" || typeof (custom_user_ID) == "int") && (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product" || what_component == "promotion")) {
        if (what_component == "category_product" || what_component == "industry_product" || what_component == "brand_product" || what_component == "promotion") {
            if (component_ID && parseInt(component_ID)) {
                sql.query("DELETE FROM tb_custom_rule_product_sorted_list WHERE composite_index = ?", composite_index, (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    if (res.affectedRows == 0) {
                        // not found product with the id
                        result({ message: "not_found" }, null);
                        return;
                    }
                    result(null, result(null, { ok: "y ", result: res }));
                });
            }
            else {
                result({ message: "missing/invalid component_ID" }, null);
            }
        }
        else if (what_component == "new_product" || what_component == "best_seller" || what_component == "recommended_product") {
            if (component_ID == "") {
                sql.query("DELETE FROM tb_custom_rule_product_sorted_list WHERE composite_index = ?", composite_index, (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    if (res.affectedRows == 0) {
                        // not found product with the id
                        result({ message: "not_found" }, null);
                        return;
                    }
                    result(null, result(null, { ok: "y ", result: res }));
                });
            }
            else {
                result({ message: "component_ID not required" }, null);
            }
        }
    }
    else {
        result({ message: "missing/invalid custom_user_ID or what_component" })
    }
};

module.exports = Product;
