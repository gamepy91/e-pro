module.exports = app => {
    const product = require("../controllers/product.controller");

    // Retrieve all Products
    app.get("/products", product.findAll);
    app.get("/products/getCustom", product.getCustom);
    app.post("/products/upsert", product.upsertCustom);
    app.post("/products/delete", product.deleteCustom);
  };
  